bplist00�_WebMainResource�	
_WebResourceTextEncodingName^WebResourceURL_WebResourceFrameName_WebResourceData_WebResourceMIMETypeUutf-8_file:///index.htmlPO+"<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <meta http-equiv="Content-Style-Type" content="text/css">
  <title></title>
  <meta name="Generator" content="Cocoa HTML Writer">
  <meta name="CocoaVersion" content="1894.6">
  <style type="text/css">
    p.p1 {margin: 0.0px 0.0px 0.0px 0.0px; font: 12.0px Helvetica}
    p.p2 {margin: 0.0px 0.0px 0.0px 0.0px; font: 12.0px Helvetica; min-height: 14.0px}
  </style>
</head>
<body>
<p class="p1">const imgUrls = ['https://wedinsider.files.wordpress.com/2012/01/2006_03_buddakan-small.jpg','https://comelite-arch.com/wp-content/uploads/2018/04/How-to-Create-a-Successful-Fine-Dining-Restaurant-0.jpg','https://restaurantindia.s3.ap-south-1.amazonaws.com/s3fs-public/content6139.jpg','https://i.ndtvimg.com/i/2015-10/restaurant_625x350_41444894654.jpg','https://d4t7t8y8xqo0t.cloudfront.net/resized/750X436/eazytrendz%2F2744%2Ftrend20200306032517.jpg','https://mumbaimirror.indiatimes.com/thumb/msid-79228089,width-1200,height-900,resizemode-4/.jpg'</p>
<p class="p1">];</p>
<p class="p2"><br></p>
<p class="p1">class Gallery extends React.Component {</p>
<p class="p1"><span class="Apple-converted-space">  </span>constructor(props) {</p>
<p class="p1"><span class="Apple-converted-space">    </span>super(props);</p>
<p class="p1"><span class="Apple-converted-space">    </span>this.state = { currentIndex: null };</p>
<p class="p1"><span class="Apple-converted-space">    </span>this.closeModal = this.closeModal.bind(this);</p>
<p class="p1"><span class="Apple-converted-space">    </span>this.findNext = this.findNext.bind(this);</p>
<p class="p1"><span class="Apple-converted-space">    </span>this.findPrev = this.findPrev.bind(this);</p>
<p class="p1"><span class="Apple-converted-space">    </span>this.renderImageContent = this.renderImageContent.bind(this);</p>
<p class="p1"><span class="Apple-converted-space">  </span>}</p>
<p class="p1"><span class="Apple-converted-space">  </span>renderImageContent(src, index) {</p>
<p class="p1"><span class="Apple-converted-space">    </span>return (</p>
<p class="p1"><span class="Apple-converted-space">      </span>&lt;div onClick={(e) =&gt; this.openModal(e, index)}&gt;</p>
<p class="p1"><span class="Apple-converted-space">        </span>&lt;img src={src} key={src} /&gt;</p>
<p class="p1"><span class="Apple-converted-space">      </span>&lt;/div&gt;</p>
<p class="p1"><span class="Apple-converted-space">    </span>)<span class="Apple-converted-space"> </span></p>
<p class="p1"><span class="Apple-converted-space">  </span>}</p>
<p class="p1"><span class="Apple-converted-space">  </span>openModal(e, index) {</p>
<p class="p1"><span class="Apple-converted-space">    </span>this.setState ({ currentIndex: index });</p>
<p class="p1"><span class="Apple-converted-space">  </span>}</p>
<p class="p1"><span class="Apple-converted-space">  </span>closeModal(e) {</p>
<p class="p1"><span class="Apple-converted-space">    </span>if (e != undefined) {</p>
<p class="p1"><span class="Apple-converted-space">      </span>e.preventDefault();</p>
<p class="p1"><span class="Apple-converted-space">    </span>}</p>
<p class="p1"><span class="Apple-converted-space">    </span>this.setState ({ currentIndex: null });</p>
<p class="p1"><span class="Apple-converted-space">  </span>}</p>
<p class="p1"><span class="Apple-converted-space">  </span>findPrev(e) {</p>
<p class="p1"><span class="Apple-converted-space">    </span>if (e != undefined) {</p>
<p class="p1"><span class="Apple-converted-space">      </span>e.preventDefault();</p>
<p class="p1"><span class="Apple-converted-space">    </span>}</p>
<p class="p1"><span class="Apple-converted-space">    </span>this.setState(prevState =&gt; ({</p>
<p class="p1"><span class="Apple-converted-space">      </span>currentIndex: prevState.currentIndex -1</p>
<p class="p1"><span class="Apple-converted-space">    </span>}));</p>
<p class="p1"><span class="Apple-converted-space">  </span>}</p>
<p class="p1"><span class="Apple-converted-space">  </span>findNext(e) {</p>
<p class="p1"><span class="Apple-converted-space">    </span>if (e != undefined) {</p>
<p class="p1"><span class="Apple-converted-space">      </span>e.preventDefault();</p>
<p class="p1"><span class="Apple-converted-space">    </span>}</p>
<p class="p1"><span class="Apple-converted-space">    </span>this.setState(prevState =&gt; ({</p>
<p class="p1"><span class="Apple-converted-space">      </span>currentIndex: prevState.currentIndex + 1</p>
<p class="p1"><span class="Apple-converted-space">    </span>}));</p>
<p class="p1"><span class="Apple-converted-space">  </span>}</p>
<p class="p1"><span class="Apple-converted-space">  </span>render() {</p>
<p class="p1"><span class="Apple-converted-space">    </span>return (</p>
<p class="p1"><span class="Apple-converted-space">      </span>&lt;div className="gallery-container"&gt;</p>
<p class="p1"><span class="Apple-converted-space">        </span>&lt;h1&gt; React JS Assignment&lt;/h1&gt;</p>
<p class="p1"><span class="Apple-converted-space">        </span>&lt;div className="gallery-grid"&gt;</p>
<p class="p1"><span class="Apple-converted-space">          </span>{imgUrls.map(this.renderImageContent)}</p>
<p class="p1"><span class="Apple-converted-space">        </span>&lt;/div&gt;</p>
<p class="p1"><span class="Apple-converted-space">        </span>&lt;GalleryModal<span class="Apple-converted-space"> </span></p>
<p class="p1"><span class="Apple-converted-space">          </span>closeModal={this.closeModal}<span class="Apple-converted-space"> </span></p>
<p class="p1"><span class="Apple-converted-space">          </span>findPrev={this.findPrev}<span class="Apple-converted-space"> </span></p>
<p class="p1"><span class="Apple-converted-space">          </span>findNext={this.findNext}<span class="Apple-converted-space"> </span></p>
<p class="p1"><span class="Apple-converted-space">          </span>hasPrev={this.state.currentIndex &gt; 0}<span class="Apple-converted-space"> </span></p>
<p class="p1"><span class="Apple-converted-space">          </span>hasNext={this.state.currentIndex + 1 &lt; imgUrls.length}<span class="Apple-converted-space"> </span></p>
<p class="p1"><span class="Apple-converted-space">          </span>src={imgUrls[this.state.currentIndex]}<span class="Apple-converted-space"> </span></p>
<p class="p1"><span class="Apple-converted-space">        </span>/&gt;</p>
<p class="p1"><span class="Apple-converted-space">      </span>&lt;/div&gt;</p>
<p class="p1"><span class="Apple-converted-space">    </span>)</p>
<p class="p1"><span class="Apple-converted-space">  </span>}</p>
<p class="p1">}</p>
<p class="p2"><br></p>
<p class="p1">class GalleryModal extends React.Component {</p>
<p class="p1"><span class="Apple-converted-space">  </span>constructor() {</p>
<p class="p1"><span class="Apple-converted-space">    </span>super();</p>
<p class="p1"><span class="Apple-converted-space">    </span>this.handleKeyDown = this.handleKeyDown.bind(this);</p>
<p class="p1"><span class="Apple-converted-space">  </span>}</p>
<p class="p1"><span class="Apple-converted-space">  </span>componentDidMount() {</p>
<p class="p1"><span class="Apple-converted-space">    </span>document.body.addEventListener('keydown', this.handleKeyDown);</p>
<p class="p1"><span class="Apple-converted-space">  </span>} <span class="Apple-converted-space"> </span></p>
<p class="p1"><span class="Apple-converted-space">  </span>componentWillUnMount() {</p>
<p class="p1"><span class="Apple-converted-space">    </span>document.body.removeEventListener('keydown', this.handleKeyDown);</p>
<p class="p1"><span class="Apple-converted-space">  </span>}</p>
<p class="p1"><span class="Apple-converted-space">  </span>handleKeyDown(e) {</p>
<p class="p1"><span class="Apple-converted-space">    </span>if (e.keyCode === 27)</p>
<p class="p1"><span class="Apple-converted-space">      </span>this.props.closeModal();</p>
<p class="p1"><span class="Apple-converted-space">    </span>if (e.keyCode === 37 &amp;&amp; this.props.hasPrev)</p>
<p class="p1"><span class="Apple-converted-space">      </span>this.props.findPrev();</p>
<p class="p1"><span class="Apple-converted-space">    </span>if (e.keyCode === 39 &amp;&amp; this.props.hasNext)</p>
<p class="p1"><span class="Apple-converted-space">      </span>this.props.findNext();</p>
<p class="p1"><span class="Apple-converted-space">  </span>}</p>
<p class="p1"><span class="Apple-converted-space">  </span>render () {</p>
<p class="p1"><span class="Apple-converted-space">    </span>const { closeModal, hasNext, hasPrev, findNext, findPrev, src } = this.props;</p>
<p class="p1"><span class="Apple-converted-space">    </span>if (!src) {</p>
<p class="p1"><span class="Apple-converted-space">      </span>console.log('whut')</p>
<p class="p1"><span class="Apple-converted-space">      </span>return null;</p>
<p class="p1"><span class="Apple-converted-space">    </span>}</p>
<p class="p1"><span class="Apple-converted-space">    </span>return (</p>
<p class="p1"><span class="Apple-converted-space">      </span>&lt;div&gt;</p>
<p class="p1"><span class="Apple-converted-space">        </span>&lt;div className="modal-overlay" onClick={closeModal}&gt;&lt;/div&gt;</p>
<p class="p1"><span class="Apple-converted-space">        </span>&lt;div isOpen={!!src} className="modal"&gt;</p>
<p class="p1"><span class="Apple-converted-space">          </span>&lt;div className='modal-body'&gt;</p>
<p class="p1"><span class="Apple-converted-space">            </span>&lt;a href="#" className='modal-close' onClick={closeModal} onKeyDown={this.handleKeyDown}&gt;&amp;times;&lt;/a&gt;</p>
<p class="p1"><span class="Apple-converted-space">            </span>{hasPrev &amp;&amp; &lt;a href="#" className='modal-prev' onClick={findPrev} onKeyDown={this.handleKeyDown}&gt;&amp;lsaquo;&lt;/a&gt;}</p>
<p class="p1"><span class="Apple-converted-space">            </span>{hasNext &amp;&amp; &lt;a href="#" className='modal-next' onClick={findNext} onKeyDown={this.handleKeyDown}&gt;&amp;rsaquo;&lt;/a&gt;}</p>
<p class="p1"><span class="Apple-converted-space">            </span>&lt;img src={src} /&gt;</p>
<p class="p1"><span class="Apple-converted-space">          </span>&lt;/div&gt;</p>
<p class="p1"><span class="Apple-converted-space">        </span>&lt;/div&gt;</p>
<p class="p1"><span class="Apple-converted-space">      </span>&lt;/div&gt;</p>
<p class="p1"><span class="Apple-converted-space">    </span>)</p>
<p class="p1"><span class="Apple-converted-space">  </span>}</p>
<p class="p1">}</p>
<p class="p2"><br></p>
<p class="p1">ReactDOM.render(&lt;Gallery /&gt;, document.querySelector('.gallery-container'));</p>
</body>
</html>
Ytext/html    ( F U l ~ � � � �+�                           +�